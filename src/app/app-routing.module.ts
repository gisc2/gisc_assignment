import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { Assignment11Component } from './beginner1-1/assignment1-1.component';
import { Assignment12Component } from './beginner1-2/assignment1-2.component';
import { Assignment13Component } from './beginner1-3/assignment1-3.component';
import { LocatorComponent } from './beginner2/locator.component';
import { MapComponent } from './beginner3/map.component';
import { Assignment4Component } from './beginner4/assignment4.component';
import { Intermediate1Component } from './intermediate1/intermediate1.component';
import { Intermediate2Component } from './intermediate2/intermediate2.component';
import { Intermediate4Component } from './intermediate4/intermediate4.component';
import { Intermediate5Component } from './intermediate5/intermediate5.component';
import { Intermediate6Component } from './intermediate6/intermediate6.component';
import { Intermediate7Component } from './intermediate7/intermediate7.component';

const routes: Routes = [
  {
    path: 'beginner1-1',
    component: Assignment11Component,
  },
  {
    path: 'beginner1-2',
    component: Assignment12Component,
  },
  {
    path: 'beginner1-3',
    component: Assignment13Component,
  },
  {
    path: 'beginner2',
    component: LocatorComponent,
  },
  {
    path: 'beginner3',
    component: MapComponent,
  },
  {
    path: 'beginner4',
    component: Assignment4Component,
  },
  {
    path: 'intermediate1',
    component: Intermediate1Component,
  },
  {
    path: 'intermediate2',
    component: Intermediate2Component,
  },
  {
    path: 'intermediate4',
    component: Intermediate4Component,
  },
  {
    path: 'intermediate5',
    component: Intermediate5Component,
  },
  {
    path: 'intermediate6',
    component: Intermediate6Component,
  },
  {
    path: 'intermediate7',
    component: Intermediate7Component,
  },
  {
    path: '',
    component: MainComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
