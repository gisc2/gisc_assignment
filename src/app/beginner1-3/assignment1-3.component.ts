import { Component } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-assignment1-3',
  templateUrl: './assignment1-3.component.html',
  styleUrls: ['./assignment1-3.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class Assignment13Component {
  name: string = "";
  comment: string = "";
  time: string = "";
  editIndexAt: number = -1;
  comments: { name: string, comment: string, time: string }[] = [];

  updateComment(name: string, comment: string, time: string) {
    if (name == "" || comment == "") {
      return;
    }
    if (this.editIndexAt >= 0) {
      this.comments.splice(this.editIndexAt, 1, { name, comment, time });
      this.editIndexAt = -1;
      this.edtied();
      return;
    }
    time = new Date().toLocaleString(
      'en-us', { month: 'short', day: 'numeric', year: 'numeric', hour: '2-digit', minute: '2-digit', hour12: true });
    this.comments.push({ name, comment, time });
    this.added();
    this.name = ""
    this.comment = ""
  }

  editComment(index: number) {
    this.name = this.comments[index].name;
    this.comment = this.comments[index].comment;
    this.time = this.comments[index].time;;
    this.editIndexAt = index;
  }

  deleteComment(index: number) {
    this.comments.splice(index, 1);
  }

  constructor(private confirmationService: ConfirmationService, private messageService: MessageService) { }

  confirm(name: string, comment: string, time: string) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.updateComment(name, comment, time);
      }
    });
  }

  added() {
    this.messageService.add({ severity: 'success', summary: 'Add Success', detail: 'Added Comment' });
  }

  edtied() {
    this.messageService.add({ severity: 'success', summary: 'Edit Success', detail: 'Edited Comment' });
  }
}
