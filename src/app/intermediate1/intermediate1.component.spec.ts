import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Intermediate1Component } from './intermediate1.component';

describe('Intermediate1Component', () => {
  let component: Intermediate1Component;
  let fixture: ComponentFixture<Intermediate1Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Intermediate1Component]
    });
    fixture = TestBed.createComponent(Intermediate1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
