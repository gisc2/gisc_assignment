import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import Graphic from '@arcgis/core/Graphic';
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";
import Polygon from '@arcgis/core/geometry/Polygon';
import SimpleFillSymbol from "@arcgis/core/symbols/SimpleFillSymbol";

@Component({
  selector: 'app-intermediate1',
  templateUrl: './intermediate1.component.html',
  styleUrls: ['./intermediate1.component.css']
})
export class Intermediate1Component implements OnInit, AfterViewInit {

  @ViewChild('mapPanel', { static: true }) mapPanel!: ElementRef;
  map: Map | null = null;
  mapView: MapView | null = null;
  states: Graphic[] = [];
  selectedState: any = null;

  fillSymbol = new SimpleFillSymbol({
    color: [0, 250, 230, 0.5],
    outline: {
      color: [0, 250, 230, 0.5],
      width: 1,
    }
  });

  ngOnInit(): void {
    this.map = new Map({
      basemap: 'topo-vector',
    });

    this.mapView = new MapView({
      container: this.mapPanel.nativeElement,
      map: this.map,
      center: [-100, 39], // [longitude, latitude]
      zoom: 4
    });
  }

  ngAfterViewInit(): void {
    const stateLayer = new FeatureLayer({
      url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/USA/MapServer/2',
    });

    this.map!.add(stateLayer);

    const query = stateLayer.createQuery();
    query.where = '1=1';
    query.outFields = ['*'];
    query.returnGeometry = true;

    stateLayer.queryFeatures(query).then((response) => {
      this.states = response.features;
    });
  }

  onClick(){
    this.mapView!.graphics.removeAll();

    if(!this.selectedState){
      return;
    }

    const polygon = new Polygon({
      rings: this.selectedState.geometry.rings,
    });

    const polygonGraphic = new Graphic({
      geometry: polygon,
      symbol: this.fillSymbol,
    });

    this.mapView!.graphics.add(polygonGraphic);
    this.mapView!.goTo(polygon.centroid);
    this.mapView!.extent = polygon.extent.expand(1.5);
  }
}
