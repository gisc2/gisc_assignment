import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import LayerList from "@arcgis/core/widgets/LayerList";
import TileLayer from "@arcgis/core/layers/TileLayer";
import MapImageLayer from "@arcgis/core/layers/MapImageLayer";

@Component({
  selector: 'app-intermediate5',
  templateUrl: './intermediate5.component.html',
  styleUrls: ['./intermediate5.component.css']
})
export class Intermediate5Component implements OnInit, AfterViewInit {
  @ViewChild('mapPanel', { static: true }) mapPanel!: ElementRef;
  map: Map | null = null;
  mapView: MapView | null = null;

  ngOnInit(): void {
    const oceans = new TileLayer({
      url: "https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer",
      title: 'World Ocean Base',
      visible: false,
    });

    const census = new MapImageLayer({
      url: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer",
      visible: false,
    });

    this.map = new Map({
      basemap: 'topo-vector',
      layers: [oceans, census]
    });
    
    this.mapView = new MapView({
      container: this.mapPanel.nativeElement,
      map: this.map,
      center: [-105.16, 39], // [longitude, latitude]
      zoom: 4
    });

    const layerList = new LayerList({
      view: this.mapView
    });
    // Adds widget below other elements in the top right corner of the view
    this.mapView.ui.add(layerList, {
      position: "top-right"
    });
  }

  ngAfterViewInit(): void {

  }
}
