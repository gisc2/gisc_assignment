import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Intermediate5Component } from './intermediate5.component';

describe('Intermediate5Component', () => {
  let component: Intermediate5Component;
  let fixture: ComponentFixture<Intermediate5Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Intermediate5Component]
    });
    fixture = TestBed.createComponent(Intermediate5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
