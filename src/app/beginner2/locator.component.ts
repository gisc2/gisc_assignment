import { Component, Input, OnInit } from '@angular/core';
import { CustomPoint } from '../form-locator/custom-point';

@Component({
  selector: 'locator',
  templateUrl: './locator.component.html',
  styleUrls: ['./locator.component.css']
})

export class LocatorComponent implements OnInit {

  customPoint: CustomPoint = new CustomPoint();

  ngOnInit() {
    console.log('ngOnInit', this.customPoint);
  }

  changeLocate(event: CustomPoint) {
    this.customPoint = event;
    console.log('onLocate', event);
  }
}
