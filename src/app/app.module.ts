import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { LocatorComponent } from './beginner2/locator.component';
import { ButtonModule } from 'primeng/button';
import { InputNumberModule } from "primeng/inputnumber";
import { CardModule } from 'primeng/card';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { ToastModule } from 'primeng/toast';  
import { TableModule } from 'primeng/table';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { Assignment11Component } from './beginner1-1/assignment1-1.component';
import { Assignment12Component } from './beginner1-2/assignment1-2.component';
import { Assignment13Component } from './beginner1-3/assignment1-3.component';
import { FormLocatorComponent } from './form-locator/form-locator.component';
import { MapComponent } from './beginner3/map.component';
import { Assignment4Component } from './beginner4/assignment4.component';
import { Intermediate1Component } from './intermediate1/intermediate1.component';
import { Intermediate2Component } from './intermediate2/intermediate2.component';
import { Intermediate4Component } from './intermediate4/intermediate4.component';
import { Intermediate5Component } from './intermediate5/intermediate5.component';
import { Intermediate6Component } from './intermediate6/intermediate6.component';
import { Intermediate7Component } from './intermediate7/intermediate7.component';



@NgModule({
  declarations: [
    AppComponent,
    LocatorComponent,
    MainComponent,
    Assignment11Component,
    Assignment12Component,
    Assignment13Component,
    FormLocatorComponent,
    MapComponent,
    Assignment4Component,
    Intermediate1Component,
    Intermediate2Component,
    Intermediate4Component,
    Intermediate5Component,
    Intermediate6Component,
    Intermediate7Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ButtonModule,
    InputNumberModule,
    FormsModule,
    CardModule,
    ConfirmDialogModule,
    ToastModule,
    TableModule
  ],
  providers: [ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
