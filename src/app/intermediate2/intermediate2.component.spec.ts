import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Intermediate2Component } from './intermediate2.component';

describe('Intermediate2Component', () => {
  let component: Intermediate2Component;
  let fixture: ComponentFixture<Intermediate2Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Intermediate2Component]
    });
    fixture = TestBed.createComponent(Intermediate2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
