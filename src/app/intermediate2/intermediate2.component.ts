import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import Graphic from '@arcgis/core/Graphic';
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";
import SimpleFillSymbol from "@arcgis/core/symbols/SimpleFillSymbol";
import Point from '@arcgis/core/geometry/Point';
import * as geometryEngine from "@arcgis/core/geometry/geometryEngine";
import SimpleMarkerSymbol from '@arcgis/core/symbols/SimpleMarkerSymbol';
import * as closestFacility from '@arcgis/core/rest/closestFacility';
import ClosestFacilityParameters from '@arcgis/core/rest/support/ClosestFacilityParameters';
import FeatureSet from "@arcgis/core/rest/support/FeatureSet";
import SimpleLineSymbol from "@arcgis/core/symbols/SimpleLineSymbol";
import PictureMarkerSymbol from "@arcgis/core/symbols/PictureMarkerSymbol";

@Component({
  selector: 'app-intermediate2',
  templateUrl: './intermediate2.component.html',
  styleUrls: ['./intermediate2.component.css']
})
export class Intermediate2Component implements OnInit, AfterViewInit {

  @ViewChild('mapPanel', { static: true }) mapPanel!: ElementRef;
  map: Map | null = null;
  mapView: MapView | null = null;
  cities: Graphic[] = [];
  closetCities: Graphic[] = [];
  citiePaths: Graphic[] = [];
  selectedCitie: any;
  selectedCitieClone: any;

  marker = new SimpleMarkerSymbol({
    color: 'black',
    outline: {
      color: 'white',
      width: 2,
    },
  });

  closetMaker = new PictureMarkerSymbol({
    url: '../../assets/city_icon.png',
    width: '30px',
    height: '30px'
  });

  fillSymbol = new SimpleFillSymbol({
    color: [0, 150, 255, 0.5],
    outline: {
      color: [0, 150, 255, 0.5],
      width: 1,
    }
  });

  lineSymbol = new SimpleLineSymbol({
    color: 'lime',
    width: 1,
  })

  selectedlLineSymbol = new SimpleLineSymbol({
    color: 'black',
    width: 1,
  })

  ngOnInit(): void {
    this.map = new Map({
      basemap: 'topo-vector',
    });

    this.mapView = new MapView({
      container: this.mapPanel.nativeElement,
      map: this.map,
      center: [-117, 32.7], // [longitude, latitude]
      zoom: 10
    });
  }

  ngAfterViewInit(): void {
    this.mapView!.on('click', (event) => {
      //ลบกราฟิกเก่า
      this.mapView!.graphics.removeAll();

      //เคลียร์เมืองจากจุดเก่า
      this.citiePaths = [];

      //add จุดที่คลิก
      const incident = new Point({
        latitude: event.mapPoint.latitude,
        longitude: event.mapPoint.longitude,
        spatialReference: {
          wkid: 3857,
        }
      });

      //ทำหมุด
      const pointGraphic = new Graphic({
        geometry: incident,
        symbol: this.marker,
      });

      //วาดวงกลมรัศมี 20 กม.
      const incBuff: any = geometryEngine.buffer(incident, 20, "kilometers");

      const polygonGraphic = new Graphic({
        geometry: incBuff,
        symbol: this.fillSymbol,
      });

      //add หมุดและวงกลม
      this.mapView!.graphics.addMany([polygonGraphic, pointGraphic]);
      this.mapView!.goTo(incBuff.centroid);
      this.mapView!.extent = incBuff.extent.expand(1.5);

      //query เมืองในวงกลม
      const citiesLayer = new FeatureLayer({
        url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/USA/MapServer/0',
      });

      const query = citiesLayer.createQuery();
      query.geometry = incBuff;
      query.spatialRelationship = 'intersects';
      query.returnGeometry = true;

      citiesLayer.queryFeatures(query).then((response) => {
        this.cities = response.features;

        //ใส่ชื่อให้เมืองในวงกลม
        this.cities.map((citie: Graphic) => {
          citie.attributes = { name: citie.attributes.areaname };
        });

        //สร้างเส้นทางจากจุดที่คลิกไปแต่ละเมืองใกล้สุด 10 เมือง
        closestFacility.solve('https://sampleserver6.arcgisonline.com/arcgis/rest/services/NetworkAnalysis/SanDiego/NAServer/ClosestFacility',
          new ClosestFacilityParameters({
            incidents: new FeatureSet({
              features: [pointGraphic],
            }),
            facilities: new FeatureSet({
              features: this.cities,
            }),
            returnRoutes: true,
            defaultTargetFacilityCount: 10,
          })
        ).then((response: any) => {
          //เคลียร์เมืองที่ใกล้ที่สุด
          this.closetCities = [];

          //รับ features เส้นทาง 10 เมืองที่ใกล้สุด
          this.citiePaths = response.routes.features

          //วนลูปทั้ง 10 เส้นทาง
          this.citiePaths.map((citiePath: any) => {
            //ใส่สีให้แต่ละเส้นทาง
            citiePath.symbol = this.lineSymbol;

            //สร้างหมุดให้เมืองที่ใกล้สุด
            const p = citiePath.geometry.paths[0][citiePath.geometry.paths[0].length - 1];
            this.closetCities.push(new Graphic({
              geometry: new Point({
                latitude: p[1],
                longitude: p[0]
              }),
              symbol: this.closetMaker,
            }));
          });

          //add หมุดและเส้นทางลงไปในแผนที่
          this.mapView!.graphics.addMany(this.closetCities);
          this.mapView!.graphics.addMany(this.citiePaths);
        });
      });
    });
  }

  onClick() {
    //ลบเส้นทางที่เลือกเก่าทิ้ง
    this.mapView!.graphics.remove(this.selectedCitieClone);

    //ถ้ามีการเลือกเมืองที่อยู่ในตาราง...
    if (this.selectedCitie) {
      //clone เมืองที่เลือกมาอยู่ใน selectedCitieClone
      this.selectedCitieClone = this.selectedCitie.clone();
      //ให้ selectedCitieClone เป็นเส้นสีดำ
      this.selectedCitieClone.symbol = this.selectedlLineSymbol;

      //add selectedCitieClone ลงไป และซูมตำแหน่งนั้น
      this.mapView!.graphics.add(this.selectedCitieClone);
      this.mapView!.goTo(this.selectedCitieClone.geometry.extent.center);
      this.mapView!.extent = this.selectedCitieClone.geometry.extent.expand(1.6);
    }
  }
}
