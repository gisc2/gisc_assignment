import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import TileLayer from "@arcgis/core/layers/TileLayer";
import Swipe from "@arcgis/core/widgets/Swipe";

@Component({
  selector: 'app-intermediate6',
  templateUrl: './intermediate6.component.html',
  styleUrls: ['./intermediate6.component.css']
})
export class Intermediate6Component implements OnInit, AfterViewInit {
  @ViewChild('mapPanel', { static: true }) mapPanel!: ElementRef;
  map: Map | null = null;
  mapView: MapView | null = null;

  ngOnInit(): void {
    const oceans = new TileLayer({
      url: "https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer",
    });

    const world_street = new TileLayer({
      url: "https://services.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer",
    });

    this.map = new Map({
      layers: [oceans, world_street]
    });

    this.mapView = new MapView({
      container: this.mapPanel.nativeElement,
      map: this.map,
      center: [-105.16, 39], // [longitude, latitude]
      zoom: 4
    });

    const swipe = new Swipe({
      view: this.mapView,
      leadingLayers: [oceans],
      trailingLayers: [world_street],
      direction: 'horizontal',
      position: 50
    });

    this.mapView.ui.add(swipe);
  }

  ngAfterViewInit(): void {

  }
}