import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Intermediate6Component } from './intermediate6.component';

describe('Intermediate6Component', () => {
  let component: Intermediate6Component;
  let fixture: ComponentFixture<Intermediate6Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Intermediate6Component]
    });
    fixture = TestBed.createComponent(Intermediate6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
