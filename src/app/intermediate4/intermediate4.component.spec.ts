import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Intermediate4Component } from './intermediate4.component';

describe('Intermediate4Component', () => {
  let component: Intermediate4Component;
  let fixture: ComponentFixture<Intermediate4Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Intermediate4Component]
    });
    fixture = TestBed.createComponent(Intermediate4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
