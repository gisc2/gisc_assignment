import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import Graphic from '@arcgis/core/Graphic';
import FeatureSet from "@arcgis/core/rest/support/FeatureSet";
import SimpleLineSymbol from "@arcgis/core/symbols/SimpleLineSymbol";
import PictureMarkerSymbol from "@arcgis/core/symbols/PictureMarkerSymbol";
import * as route from "@arcgis/core/rest/route";
import RouteParameters from "@arcgis/core/rest/support/RouteParameters";

@Component({
  selector: 'app-intermediate4',
  templateUrl: './intermediate4.component.html',
  styleUrls: ['./intermediate4.component.css']
})
export class Intermediate4Component implements OnInit, AfterViewInit {

  @ViewChild('mapPanel', { static: true }) mapPanel!: ElementRef;
  map: Map | null = null;
  mapView: MapView | null = null;
  points: Graphic[] = [];
  paths: Graphic[] = [];
  selectedPath: Graphic = new Graphic();
  clonePath: Graphic = new Graphic();

  startMarker = new PictureMarkerSymbol({
    url: '../../assets/start_pin.png',
    width: '30px',
    height: '30px'
  });

  targetMarker = new PictureMarkerSymbol({
    url: '../../assets/target_pin.png',
    width: '30px',
    height: '30px'
  });

  lineSymbol = new SimpleLineSymbol({
    color: 'lime',
    width: 1,
  });

  selectedlLineSymbol = new SimpleLineSymbol({
    color: 'black',
    width: 1,
  })

  ngOnInit(): void {
    this.map = new Map({
      basemap: 'topo-vector',
    });

    this.mapView = new MapView({
      container: this.mapPanel.nativeElement,
      map: this.map,
      center: [-117.16, 32.7125], // [longitude, latitude]
      zoom: 15
    });
  }

  ngAfterViewInit(): void {
    this.mapView!.on('click', (event) => {
      const point = new Graphic();
      if (this.points.length == 0) {
        point.geometry = event.mapPoint;
        point.symbol = this.startMarker;
        this.points.push(point);
      } else {
        point.geometry = event.mapPoint;
        point.symbol = this.targetMarker;
        this.points.push(point);
      }

      this.mapView!.graphics.add(point);
    });
  }

  startRoute() {
    const routeParams = new RouteParameters({
      stops: new FeatureSet({
        features: [...this.points] // 2 or more 2 Point graphics required
      }),
      returnDirections: true,
    });

    route.solve("https://sampleserver6.arcgisonline.com/arcgis/rest/services/NetworkAnalysis/SanDiego/NAServer/Route", routeParams)
      .then((response) => {
        this.paths = response.routeResults[0].directions.features;

        this.paths.map((path) =>
          path.symbol = this.lineSymbol
        );

        this.mapView!.graphics.addMany(this.paths);
      });
  }

  clearAll() {
    this.mapView!.graphics.removeAll();
    this.points = [];
    this.paths = [];
  }

  clickPath() {
    this.mapView!.graphics.remove(this.clonePath);

    if (this.selectedPath) {
      this.clonePath = this.selectedPath.clone();
      this.clonePath.symbol = this.selectedlLineSymbol;
      this.mapView!.graphics.add(this.clonePath);

      this.mapView!.goTo(this.selectedPath.geometry.extent.center);
      if(this.selectedPath.geometry.extent.width != 0){
        this.mapView!.extent = this.selectedPath.geometry.extent.expand(1.6);
      }
    }
  }
}
