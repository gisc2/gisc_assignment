import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CustomPoint } from './custom-point';

@Component({
  selector: 'app-form-locator',
  templateUrl: './form-locator.component.html',
  styleUrls: ['./form-locator.component.css']
})
export class FormLocatorComponent implements OnInit {

  constructor() {}

  customPoint: CustomPoint = new CustomPoint();

  @Input() formTitle: string = 'Locator';

  @Output() onLocate: EventEmitter<CustomPoint> = new EventEmitter<CustomPoint>();

  ngOnInit() { }

  onClickLocateButton() {
    this.onLocate.emit(this.customPoint);
  }
}
