import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormLocatorComponent } from './form-locator.component';

describe('FormLocatorComponent', () => {
  let component: FormLocatorComponent;
  let fixture: ComponentFixture<FormLocatorComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormLocatorComponent]
    });
    fixture = TestBed.createComponent(FormLocatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
