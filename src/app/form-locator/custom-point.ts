export class CustomPoint {
    private _latitude: number | null = null;
    private _longitude: number | null = null;

    get latitude(): number {
        return this._latitude!;
    }

    get longitude(): number {
        return this._longitude!;
    }

    set latitude(newLatitude: number) {
        this._latitude = newLatitude;
    }

    set longitude(newLongitude: number) {
        this._longitude = newLongitude;
    }
}