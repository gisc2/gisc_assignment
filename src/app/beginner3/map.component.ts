import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import Point from '@arcgis/core/geometry/Point';
import SimpleMarkerSymbol from '@arcgis/core/symbols/SimpleMarkerSymbol';
import Graphic from '@arcgis/core/Graphic';
import { CustomPoint } from '../form-locator/custom-point';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
})

export class MapComponent implements OnInit, AfterViewInit {

  @ViewChild('mapPanel', { static: true }) mapPanel!: ElementRef;
  map: Map | null = null;
  mapView: MapView | null = null;
  point = new Point();

  marker = new SimpleMarkerSymbol({
    color: [226, 119, 40],
    outline: {
      color: [255, 255, 255],
      width: 2,
    },
  });


  ngOnInit(): void {
    this.map = new Map({
      basemap: 'topo-vector',
    });

    this.mapView = new MapView({
      container: this.mapPanel.nativeElement,
      map: this.map,
      center: [100.5408754, 13.7030248], // [longitude, latitude]
      zoom: 10, // zoom level
    });
  }

  ngAfterViewInit(): void {
  }

  changeLocate(event: CustomPoint) {
    this.mapView!.graphics.removeAll();

    this.point.latitude = event.latitude;

    this.point.longitude = event.longitude;


    const pointGraphic: Graphic = new Graphic({
      geometry: this.point,
      symbol: this.marker,
    });

    this.mapView!.graphics.add(pointGraphic);
    this.mapView!.goTo(this.point);
  }
}