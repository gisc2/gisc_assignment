import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import Point from '@arcgis/core/geometry/Point';
import SimpleMarkerSymbol from '@arcgis/core/symbols/SimpleMarkerSymbol';
import Graphic from '@arcgis/core/Graphic';
import { CustomPoint } from '../form-locator/custom-point';
import { FormLocatorComponent } from '../form-locator/form-locator.component';
import MapImageLayer from "@arcgis/core/layers/MapImageLayer";
import * as identify from "@arcgis/core/rest/identify";
import IdentifyParameters from "@arcgis/core/rest/support/IdentifyParameters";
import Polygon from '@arcgis/core/geometry/Polygon';
import SimpleFillSymbol from "@arcgis/core/symbols/SimpleFillSymbol";

@Component({
  selector: 'app-assignment4',
  templateUrl: './assignment4.component.html',
  styleUrls: ['./assignment4.component.css']
})
export class Assignment4Component implements OnInit, AfterViewInit {

  @ViewChild(FormLocatorComponent)
  formLatorComponent: FormLocatorComponent = new FormLocatorComponent;

  @ViewChild('mapPanel', { static: true }) mapPanel!: ElementRef;
  map: Map | null = null;
  mapView: MapView | null = null;
  point = new Point();

  marker = new SimpleMarkerSymbol({
    color: [226, 119, 40],
    outline: {
      color: [255, 255, 255],
      width: 2,
    },
  });

  fillSymbol = new SimpleFillSymbol({
    color: [93, 250, 50, 0.3],
    outline: {
      color: [93, 250, 50, 0.3],
      width: 1,
    }
  });

  ngOnInit(): void {
    this.map = new Map({
      basemap: 'topo-vector'
    });

    this.mapView = new MapView({
      container: this.mapPanel.nativeElement,
      map: this.map,
      center: [-105, 39], // [longitude, latitude]
      zoom: 7, // zoom level
    });

    const layer = new MapImageLayer({
      url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer/',
    });

    this.map.add(layer);
  }

  ngAfterViewInit(): void {
    this.mapView!.on('click', (event) => {
      const customPoint: CustomPoint = new CustomPoint();

      customPoint.latitude = event.mapPoint.latitude;
      customPoint.longitude = event.mapPoint.longitude;

      this.formLatorComponent.customPoint.latitude = customPoint.latitude;
      this.formLatorComponent.customPoint.longitude = customPoint.longitude;

      this.changeLocate(customPoint);

      const params = new IdentifyParameters();
      params.tolerance = 1;
      params.layerIds = [3];
      params.geometry = event.mapPoint;
      params.mapExtent = this.mapView!.extent;
      params.returnGeometry = true;

      identify.identify("https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer", params).then((response: any) => {
        let feature = response.results[0].feature;

        const pop2007 = feature.attributes.POP2007.toLocaleString("en-US");
        const area = feature.attributes.Shape_Area.toFixed(3);

        feature.popupTemplate = {
          title: feature.attributes.STATE_NAME,
          content: '<p>Population (2007): ' + pop2007 + '</p>'
            + '<p>Area: ' + area + '</p>'
        };

        this.mapView!.openPopup({
          features: [feature],
          location: event.mapPoint,
        });

        const polygon = new Polygon({
          rings: feature.geometry.rings,
          spatialReference: {
            wkid: 3857
          }
        });

        const polygonGraphic: Graphic = new Graphic({
          geometry: polygon,
          symbol: this.fillSymbol,
        });

        this.mapView!.graphics.add(polygonGraphic);
      })
    });
  }

  changeLocate(event: CustomPoint) {
    this.mapView!.graphics.removeAll();

    this.point.latitude = event.latitude;
    this.point.longitude = event.longitude;

    const pointGraphic: Graphic = new Graphic({
      geometry: this.point,
      symbol: this.marker,
    });

    this.mapView!.graphics.add(pointGraphic);
    this.mapView!.goTo(this.point);
  }
}
