import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import * as urlUtils from "@arcgis/core/core/urlUtils";
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";

@Component({
  selector: 'app-intermediate7',
  templateUrl: './intermediate7.component.html',
  styleUrls: ['./intermediate7.component.css']
})
export class Intermediate7Component implements OnInit, AfterViewInit {
  @ViewChild('mapPanel', { static: true }) mapPanel!: ElementRef;
  map: Map | null = null;
  mapView: MapView | null = null;

  ngOnInit(): void {
    this.map = new Map({});

    this.mapView = new MapView({
      container: this.mapPanel.nativeElement,
      map: this.map,
      center: [105, 13], // [longitude, latitude]
      zoom: 4
    });

    urlUtils.addProxyRule({
      urlPrefix: 'https://gisserv1.cdg.co.th/arcgis/rest/services',
      proxyUrl: 'https://localhost:5001/api/appproxy',
    });
  }

  ngAfterViewInit(): void {
    const layer = new FeatureLayer({
      url: 'https://gisserv1.cdg.co.th/arcgis/rest/services/AtlasX/AtlasX_Secure/MapServer'
    });

    this.map!.add(layer);
  }
}
