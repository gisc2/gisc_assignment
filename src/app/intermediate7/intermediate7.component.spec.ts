import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Intermediate7Component } from './intermediate7.component';

describe('Intermediate7Component', () => {
  let component: Intermediate7Component;
  let fixture: ComponentFixture<Intermediate7Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Intermediate7Component]
    });
    fixture = TestBed.createComponent(Intermediate7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
